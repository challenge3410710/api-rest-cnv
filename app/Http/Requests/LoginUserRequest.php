<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class LoginUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return json with validation errors.
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|exists:users',
            'password' => 'required',
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $error = [
            'code' => 401,
            'message' => 'errores de validación',
            'details' => $errors
        ];
        throw new HttpResponseException(response()->json([
            'success' => false,
            'value' => [],
            'error' => $error,
        ], JsonResponse::HTTP_UNAUTHORIZED));
    }

    public function messages()
    {
        return [
            'email.required' => 'El email es obligatorio',
            'email.email' => 'El email ingresado no es correcto',
            'email.exists' => 'el email no se encuentra registrado',
            'password.required' => 'El password es obligatorio',
        ];
    }
}
