<?php

namespace App\Repositories;

interface IFavoriteRepository
{
    public function saveFavorite($data);
    public function getFavorites($userId);
}