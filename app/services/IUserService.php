<?php

namespace App\services;

interface IUserService
{   
    public function saveUser($data);
    public function health();
    public function getUserByEmail($email);
    
}