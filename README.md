To start the API we can do it in two ways:

1)
- It should be located in the root folder
- Execute the following command(s)

   1)  `docker compose up -d` 
   2)  `docker exec api-rest-cnv-app-1 php artisan migrate`
   3)  `docker exec api-rest-cnv-app-1 php artisan passport:install --force`

    "The API will be available in http://localhost:8000"

2)
- It should be located in the root folder
- Execute the following command(s)

   1) Make sure you have execute permissions on the following file `run-container.sh`
      You can grant these permissions with the following command:
        `chmod +x run-container.sh`

   2)  `./run-container.sh` 
   
   (make sure you have execute permissions on the file )
    "The API will be available in http://localhost:8000"

## Usage

To get started, make sure you have [Docker installed](https://docs.docker.com/docker-for-mac/install/) on your system, and then clone this repository.

Next, navigate in your terminal to the directory you cloned this, and spin up the containers for the web server by running `docker-compose up -d --build app`.

After that completes, follow the steps from the [src/README.md](src/README.md) 

The following are built for our web server, with their exposed ports detailed:

- **mysql** - `:3306`
- **php** - `:8000`

## Persistent MySQL Storage

By default, whenever you bring down the Docker network, your MySQL data will be removed after the containers are destroyed. If you would like to have persistent data that remains after bringing containers down and back up, do the following:

1. Create a `mysql` folder in the project root, alongside the `nginx` and `src` folders.
2. Under the mysql service in your `docker-compose.yml` file, add the following lines:

```
volumes:
  - ./mysql:/var/lib/mysql
```
