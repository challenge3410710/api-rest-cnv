<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class GetGifsUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'query' => 'required|string|max:30',
            'limit' => 'numeric|min:0|max:100',
            'offset' => 'numeric|min:0|max:50',
        ];
    }

    protected function failedValidation(Validator $validator)
    {  
        $errors = $validator->errors();
        $error = [
            'code' => 400,
            'message' => 'errores de validación',
            'details' => $errors
        ];
        throw new HttpResponseException(response()->json([
            'success' => false,
            'value' => [],
            'error' => $error,
        ], JsonResponse::HTTP_BAD_REQUEST));
    }

    public function messages()
    {
        return [
            'query.required' => 'Parametro query es obligatorio',
            'query.string' => 'Parametro query debe contener sólo caracteres alfanumericos',
            'query.max' => 'Parametro query debe ser de una longitud menor/igual a 30',

            'limit.numeric' => 'Parametro limit debe contener sólo caracteres numericos',
            'limit.min' => 'Parametro limit debe ser mayor/igual a 0',
            'limit.max' => 'Parametro limit debe ser menor/igual a 100',

            'offset.numeric' => 'Parametro offset debe contener sólo caracteres numericos',
            'offset.min' => 'Parametro offset debe ser mayor/igual a 0',
            'offset.max' => 'Parametro offset debe ser menor/igual a 50',
        ];
    }
}
