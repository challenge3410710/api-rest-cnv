<?php

namespace App\Repositories;
use App\Models\Activity;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Exception;

class IActivityRepositoryImp implements IActivityRepository
{
    public function saveActivity($data){
        
        $activity = Activity::create($data);
        $dataResp = ["favorite" => $activity];

        return response()->json(['sucess' => true, 'value' => $dataResp, "error" => []], 200);
    }
}