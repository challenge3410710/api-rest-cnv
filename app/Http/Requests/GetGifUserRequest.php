<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class GetGifUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'gif_id' => 'alpha_num',
        ];
    }

    protected function failedValidation(Validator $validator)
    {  
        $errors = $validator->errors();
        $error = [
            'code' => 400,
            'message' => 'errores de validación',
            'details' => $errors
        ];
        throw new HttpResponseException(response()->json([
            'success' => false,
            'value' => [],
            'error' => $error,
        ], JsonResponse::HTTP_BAD_REQUEST));
    }

    public function messages()
    {
        return [
            'gif_id.alpha_num' => 'id debe tener un valor alfanúmerico',
        ];
    }
}
