<?php

namespace App\Repositories;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class INonverbalCommunicationImp implements INonverbalCommunication
{
    protected $client;
    protected $baseUrl;
    protected $tokenApi;
    
    public function __construct()
    {   
        $this->baseUrl = env('GIF_API_BASE_URL');
        $this->tokenApi = env('GIF_API_TOKEN');
        
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
        ]);
    }

    public function getGif($id){
      
            $urlApi = $this->baseUrl.'/v1/gifs/'.$id.'?api_key='.$this->tokenApi;
            $responseApi = $this->client->get( $urlApi );
            $response = json_decode($responseApi->getBody()->getContents(), true);
            
            $error = json_encode([]);
            $dataResp = $response['data'];
            
            return response()->json(['sucess' => true, 'value' => $dataResp, "error" => $error ], 200);
    }

    public function getGifs($data){
     
            $limit = $data["limit"] ? $data["limit"] : env('API_LIMIT_DEFAULT');
            $offset = $data["offset"] ? $data["offset"] : env('API_OFFSET_DEFAULT');
            $query = $data["query"];

            $urlApi = $this->baseUrl.'/v1/gifs/search?api_key='.$this->tokenApi.'&q='.$query.'&limit='.$limit.'&offset='.$offset;
            $responseApi = $this->client->get( $urlApi );   
            $response = json_decode($responseApi->getBody()->getContents(), true);
            $dataResp = $response['data'];

            return response()->json(['sucess' => true, 'value' => $dataResp, "error" => []], 200);
    }
}