#!/bin/bash

# Levanta el contenedor en segundo plano
docker compose up -d

# Espera unos segundos para asegurarse de que el contenedor esté completamente levantado
sleep 15

# Ejecuta los comando Artisan dentro del contenedor
docker exec api-rest-cnv-app-1 php artisan migrate 

docker exec api-rest-cnv-app-1 php artisan passport:install --force 

echo "La aplicación está disponible en http://localhost:8000"