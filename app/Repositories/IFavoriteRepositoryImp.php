<?php

namespace App\Repositories;

use App\Models\Favorite;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Exception;
use App\Models\User;
use App\Models\Registro;

class IFavoriteRepositoryImp implements IFavoriteRepository
{
    public function saveFavorite($data){
        
        $favorite = Favorite::create($data);
     
        $dataResp = ["favorite" => $favorite];
        return response()->json(['sucess' => true, 'value' => $dataResp, "error" => []], 200);
    }

    public function getFavorites($userId){
      
        $favoritos = Favorite::where('user_id', $userId)->get();
        $dataResp = ["favorite" => $favoritos];
        
        return response()->json(['sucess' => true, 'value' => $dataResp, "error" => []], 200);    
    }
}