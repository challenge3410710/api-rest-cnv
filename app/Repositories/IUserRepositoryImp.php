<?php

namespace App\Repositories;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Exception;

class IUserRepositoryImp implements IUserRepository
{
    public function saveUser($data){
        
        $user = User::create($data);
        $token = $user->createToken('API Token')->accessToken;

        $dataResp = ["user" => $user, "token"=> $token];
        $response = response()->json(['sucess' => true, 'value' => $dataResp, "error" => [] ], 200);

        return $response;
    }

    public function getUserByEmail($email){
        
        $user = User::where('email', $email)->first();
        
        return $user;
    }

    
    
    public function health(){
        
        try {
            DB::connection()->getPdo();

            $success_app = ["La aplicacion esta arriba"];
            $success_db = ['La base de datos responde ok'];
            $dataResp = ["app" => $success_app, 'db' => $success_db];
        
            return response()->json([
                'sucess' => true, 
                'value' => $dataResp, 
                "error" => []
            ], JsonResponse::HTTP_OK);

        } catch (QueryException $e) {
            $errors = ['db' => ['No responde la DB']];
            $error = [
                'code' => 500,
                'message' => 'internal error',
                'details' => $errors
            ];
            return response()->json([
                'success' => false,
                'value' => [],
                'error' => $error
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}