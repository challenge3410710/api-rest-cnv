<?php

namespace App\services;

use GuzzleHttp\Client;
use App\Repositories\IActivityRepository;

class IActivityServiceImp implements IActivityService  
{   
    protected $activityRepository;

    public function __construct(IActivityRepository $activityRepository){
        $this -> activityRepository = $activityRepository;
    }
    
    public function saveActivity($data){
        return $this -> activityRepository -> saveActivity($data);
    }
}
