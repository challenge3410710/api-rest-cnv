<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\services\IGifService;
use App\services\IActivityService;
use App\services\IFavoriteService;
use App\Http\Requests\GetGifsUserRequest;
use App\Http\Requests\SaveFavoriteUserRequest;
use App\Http\Requests\GetGifUserRequest;
use App\Http\Controllers\Controller;

class GifController extends Controller
{
    protected $gifService;
    protected $favoriteService;
    protected $activityService;

    public function __construct(IGifService $gifService, IFavoriteService $favoriteService, IActivityService $activityService)
    {
        $this->gifService = $gifService;
        $this->favoriteService = $favoriteService;
        $this->activityService = $activityService;
    }

    /**
     * Obtiene el gif correspondiente a un gif_id     
     * 
     * @quertParam id del gif a obtener.
     * @return json Con un objeto Gif
     */
    public function getGif(GetGifUserRequest $request){

            $response = $this -> gifService -> getGif($request->gif_id); 
            
            return $response;
    }   

    /**
     * Obtiene todos los gifs que responden a una palabra determinada.
     *
     * @param query string palabra/frase patron para buscar gifs.
     * @param limit
     * @param offset
     * @return json Con un objeto Gif
     */
    public function getGifs(GetGifsUserRequest $request){
     
            $data['query'] = $request->get('query');
            $data['limit'] =  $request->limit;
            $data['offset'] =  $request->offset;
            $response = $this -> gifService -> getGifs($data);
    
            return $response; 
    }

    /**
     * Graba el gif favorito.
     *
     * @param gif_id
     * @param alias
     * @param user_id 
     * @return json Con un objeto Gif
     */

     public function saveFavorite(SaveFavoriteUserRequest $request){
   
            $data["user_id"] = $request->get('user_id');
            $data["cnv_id"] = $request->get('gif_id');
            $data["alias"] = $request->get('alias');
            $data["type"] = 'gif';
            $response = $this -> favoriteService -> saveFavorite($data);
      
            return $response;
    }

    public function getFavorites(Request $request){
       
        $id = $request -> user() -> id;
        $response = $this -> favoriteService -> getFavorites($id);
  
        return $response;
}
}
