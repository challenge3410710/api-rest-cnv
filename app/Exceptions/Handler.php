<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\ClientException;
use PDOException;
use Exception;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * the exception handling callbacks for the application.
     */
    public function register(): void
    {   
        $this->renderable(function (AuthenticationException $e, $request) {

            $errors = ['password' => ['el password no es valido']];
            $error = [
                'code' => 401,
                'message' => 'unauthorized',
                'details' => $errors
            ];
            $response = response()->json([
                'success' => false,
                'value' => [],
                'error' => $error,
            ], JsonResponse::HTTP_UNAUTHORIZED);

            return $response;
        });

        $this->renderable(function (PDOException $e, $request) {
            
            if ($e->getCode() === 23000) {
                $errors = ['query' => ['error al intentar registrar el/los recurso/s']];
            $error = [
                'code' => 23000,
                'message' => 'user_id no registrado',
                'details' => $errors
            ];
            $response = response()->json([
                'success' => false,
                'value' => [],
                'error' => $error,
            ], JsonResponse::HTTP_NOT_FOUND);

            return $response;
            }

            if ($e->getCode() === 2002) {
                $errors = ['db' => ['DB-PDO[2002] Connection refused']];
                $error = [
                    'code' => 2002,
                    'message' => 'error interno',
                    'details' => $errors
                ];
                $response = response()->json([
                    'success' => false,
                    'value' => [],
                    'error' => $error,
                ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);

            return $response;

            }          
        });
    
        $this->renderable(function (QueryException $e, $request) {

            if ($e->getCode() === '23000') {
                $errors = ['user_id' => ['el user_id es invalido']];
                
                $error = [
                    'code' => 400,
                    'message' => 'error al intentar guardar el registro',
                    'details' => $errors
                ];

                return response()->json([
                    'success' => false,
                    'value' => [],
                    'error' => $error,
                ], JsonResponse::HTTP_BAD_REQUEST);
            }
            if ($e->getCode() === '2002') {
                $errors = ['user_id' => ['el user_id es invalido']];
                
                $error = [
                    'code' => 400,
                    'message' => 'error al intentar guardar el registro',
                    'details' => $errors
                ];
                return response()->json([
                    'success' => false,
                    'value' => [],
                    'error' => $error,
                ], JsonResponse::HTTP_BAD_REQUEST);

            } 
        });

        $this->renderable(function (ClientException $e, $request) {
            $statusCode = $e->getResponse()->getStatusCode();
            if ($statusCode == 404) {
                $errors = ['gif_id' => ['gif_id no existente']];
                $error = [
                    'code' => $statusCode,
                    'message' => 'recurso no encontrado',
                    'details' => $errors
                ];
                return response()->json([
                    'success' => false,
                    'value' => [],
                    'error' => $error,
                ], JsonResponse::HTTP_NOT_FOUND);
            }
            if ($statusCode == 400) {
                $errors = ['gif_id' => ['el gif_id no posee un formato valido']];
                $error = [
                    'code' => $statusCode,
                    'message' => 'error de validación',
                    'details' => $errors
                ];
                return response()->json([
                    'success' => false,
                    'value' => [],
                    'error' => $error,
                ], JsonResponse::HTTP_BAD_REQUEST);
            } 
        });

        $this->renderable(function (Exception $e, $request) { 
         
            $errors = ['app' => ['internal server error']];
            $error = [
                'code' => 9999,
                'message' => 'internal server error',
                'details' => $errors
            ];
            $response = response()->json([
                'success' => false,
                'value' => [],
                'error' => $error,
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);

            return $response;
        });
    }
}   