<?php

namespace App\services;

interface IFavoriteService
{   
    public function saveFavorite($data);
    public function getFavorites($user_id);
}