<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Redirecting to unauthenticated user.
     */
    protected function redirectTo(Request $request): ? string
    {
        return $request->expectsJson() ? null : route('unauthenticated');
    }
}
