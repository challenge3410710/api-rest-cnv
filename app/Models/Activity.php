<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens; 
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Activity extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
      //  'user_id',
      //  'activity_type',
        'request',
        'response',    
      // 'http_code_response',
      // 'origen_ip',
    ]; 
}
