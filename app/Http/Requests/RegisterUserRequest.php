<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return json with validation errors.
     */
    public function rules(): array
    {       
        return [
            'name' => 'required|string|max:15',
            'email' => 'required|email|min:3|max:20|unique:users',
            'password' => 'required|string|min:4|max:20',
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {  
        $errors = $validator->errors();
        $error = [
            'code' => 400,
            'message' => 'errores de validación',
            'details' => $errors
        ];
        throw new HttpResponseException(response()->json([
            'success' => false,
            'value' => [],
            'error' => $error,
        ], JsonResponse::HTTP_BAD_REQUEST));
    }

    public function messages()
    {
        return [
            'email.required' => 'El email es obligatorio',
            'email.email' => 'El email ingresado es invalido',
            'email.min' => 'El email debe ser de una longitud mayor/igual a 3',
            'email.max' => 'El email debe ser de una longitud menor/igual a 20',
            'email.unique' => 'El email ya esta en uso',

            'name.required' => 'El nombre es obligatorio',
            'name.string' => 'El nombre debe contener sólo caracteres alfanumericos',
            'name.max' => 'El nombre no puede tener una longitud mayor a 15',
            
            'password.required' => 'El password es obligatorio',
            'password.string' => 'El password debe contener sólo caracteres alfanumericos',
            'password.min' => 'El password debe tener una longitud minima de 4',
            'password.max' => 'El password debe ser de una longitud menor/igual a 20',
        ];
    }
}
