<?php

namespace App\Providers;

use App\Repositories\IFavoriteRepository;
use App\Repositories\IFavoriteRepositoryImp;
use App\Repositories\IUserRepository;
use App\Repositories\IUserRepositoryImp;
use App\Repositories\IActivityRepository;
use App\Repositories\IActivityRepositoryImp;
use App\Repositories\INonverbalCommunication;
use App\Repositories\INonverbalCommunicationImp;
use Illuminate\Support\ServiceProvider;
use App\services\IGifService;
use App\services\IGifServiceImp;
use App\services\IUserService;
use App\services\IUserServiceImp;
use App\services\IFavoriteService;
use App\services\IFavoriteServiceImp;
use App\services\IActivityService;
use App\services\IActivityServiceImp;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {   
        $this->app->bind(IGifService::class, IGifServiceImp::class);
        $this->app->bind(IUserService::class, IUserServiceImp::class);
        $this->app->bind(INonverbalCommunication::class, INonverbalCommunicationImp::class);    
        $this->app->bind(IFavoriteService::class, IFavoriteServiceImp::class);
        $this->app->bind(IFavoriteRepository::class, IFavoriteRepositoryImp::class);
        $this->app->bind(IUserRepository::class, IUserRepositoryImp::class);  
        $this->app->bind(IActivityService::class, IActivityServiceImp::class);
        $this->app->bind(IActivityRepository::class, IActivityRepositoryImp::class);   
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        
    }
}
