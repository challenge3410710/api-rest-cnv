<?php

namespace App\services;

use GuzzleHttp\Client;
use App\Repositories\INonverbalCommunication;

class IGifServiceImp implements IGifService  
{   
    protected $nonverbalCommunication;

    public function __construct(INonverbalCommunication $nonverbalCommunication){
        $this -> nonverbalCommunication = $nonverbalCommunication;
    }
    
    public function getGif($id){  
        return $this -> nonverbalCommunication -> getGif($id);
    }

    public function getGifs($data){
        return $this -> nonverbalCommunication -> getGifs($data);
    }
}
