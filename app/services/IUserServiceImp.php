<?php

namespace App\services;

use GuzzleHttp\Client;
use App\Repositories\IUserRepository;

class IUserServiceImp implements IUserService  
{   
    protected $userRepository;

    public function __construct(IUserRepository $userRepository){
        $this -> userRepository = $userRepository;
    }
    public function saveUser($data){
        return $this -> userRepository -> saveUser($data);
    }
    public function health(){
        return $this -> userRepository -> health();
    }
    public function getUserByEmail($email){
        return $this -> userRepository -> getUserByEmail($email); 
    }
}
