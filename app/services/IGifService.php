<?php

namespace App\services;

interface IGifService
{   
    public function getGif($id);
    public function getGifs($data);
}