FROM php:8.2-cli

WORKDIR /var/www/html

COPY . /var/www/html

RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

RUN docker-php-ext-install pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install

# Genera una clave de aplicación única
RUN php artisan key:generate

# Genero la documentacion swagger
# RUN php artisan l5-swagger:generate

# Expone el puerto 8000 para acceder a la aplicación
EXPOSE 8000

# Comando para ejecutar el servidor web cuando se inicie el contenedor
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=8000"]
