<?php

namespace App\Exceptions;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use PDOException;
use Throwable;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Connection;
use Exception;
class CustomExceptionHandler extends ExceptionHandler
{
    /**
     * Render the exception handling callbacks for the application.
     */
    public function render($request, Throwable $exception)
    {   
        if ($exception instanceof QueryException && $exception->getPrevious() instanceof PDOException) {
            $pdoException = $exception->getPrevious();
            $errorCode = $pdoException->getCode();
            if ($errorCode === '2002') {
                Log::error('Error de conexión a la base de datos: Conexión rechazada');
                return response()->json(['error' => 'Conexión rechazada a la base de datos'], 304);
            }
        }
        
        if ($exception instanceof PDOException) {
            $message = 'PDOException desde metodo render(): ' . $exception->getMessage();
            
            $errorCode = $pdoException->getCode();
            if ($errorCode === '2002') {
                Log::error('Error de conexión a la base de datos: Conexión rechazada');
                return response()->json(['error' => 'Conexión rechazada a la base de datos'], 304);
            }
            
            return response()->json(['error JOSE' => $message], 500);
        }

        if ($exception instanceof QueryException) {
            $message = 'QueryException desde metodo render(): ' . $exception->getMessage();
            return response()->json(['error JOSE' => $message], 500);
        }
        
        if ($exception instanceof Exception) {
            $message = 'Exception desde metodo render(): ' . $exception->getMessage();
            return response()->json(['error JOSE' => $message], 500);
        }


        return parent::render($request, $exception);
    }

   
}
