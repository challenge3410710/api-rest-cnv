<?php

namespace App\services;

use App\Repositories\IFavoriteRepository;

class IFavoriteServiceImp implements IFavoriteService  
{   
    protected $favoriteRepository;

    public function __construct(IFavoriteRepository $favoriteRepository){
        $this -> favoriteRepository = $favoriteRepository;
    }
   
    public function saveFavorite($data){
        return $this -> favoriteRepository -> saveFavorite($data);
    }

    public function getFavorites($user_id){
        return $this -> favoriteRepository -> getFavorites($user_id);    
    }
}
