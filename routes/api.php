<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GifController;
use App\Http\Controllers\UserController;

use Illuminate\Http\JsonResponse;

// use Laravel\Passport\RouteRegistrar;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {

    Route::get('/health', [UserController::class, 'health']);
    Route::middleware('save.activity')->post('/register', [UserController::class, 'register']);
    Route::middleware('save.activity')->post('/login', [UserController::class, 'login']);

    Route::middleware('auth:api')->group(
       function(){
            Route::get('/test', [UserController::class,'test']);
            Route::middleware('save.activity')->get('/cnv/gifs', [GifController::class, 'getGifs']);
            Route::middleware('save.activity')->get('/cnv/gif', [GifController::class, 'getGif']);
            Route::middleware('save.activity')->post('/cnv/gif/favorite', [GifController::class, 'saveFavorite']);
            Route::middleware('save.activity')->get('/cnv/gif/favorite-list', [GifController::class, 'getFavorites']);
        }
    );

    Route::get('/unauthenticated', function (Request $request) {
        $errors = ['request' => ['errores de autenticación']];
        $error = [
            'code' => 401,
            'message' => 'unauthenticated',
            'details' => $errors
        ];
        return response()->json([
            'success' => false,
            'value' => [],  
            'error' => $error,
        ], JsonResponse::HTTP_UNAUTHORIZED); 
    })->name('unauthenticated');
});