<?php

namespace App\Repositories;

interface IUserRepository
{
    public function saveUser($data);
    public function health();
    public function getUserByEmail($email);
}