<?php

namespace App\Repositories;

interface INonverbalCommunication
{
    public function getGif($id);
    public function getGifs($data);
}