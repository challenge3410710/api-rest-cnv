<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use App\services\IUserService;

class UserController extends Controller
{   
    protected $userService;

    public function __construct(IUserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Registra un usuario de la API
     *
     * @queryParam name string Nombre del usuario.
     * @queryParam email string Mail del usuario.
     * @queryParam password string Contraseña del usuario.
     * @return json Con un objeto user y el token autorizador
     */
    public function register(RegisterUserRequest $request)
    {   
            $data = $request->only(['name', 'email']);
            $data['password'] = bcrypt($request->password);      
            $response = $this->userService->saveUser($data);
            
            return  $response;
    }

    /**
     * Login del usuario en la API
     *
     * @param email string Mail del usuario.
     * @param password string Contraseña del usuario.
     * @return json Con un objeto user y el token autorizador
     */
    public function login(LoginUserRequest $request)
    {   
        $email =   $request->email;
        $user = $this->userService->getUserByEmail($email);
   
        $token = $user->createToken('API Token')->accessToken;
        $response = response()->json(['sucess' => true, 'value' => $token, "error" => [] ], 200);
       
        return $response;
    }

     /**
     *  Test token, obtiene todos los usarios
     *
     * @return json Con todos los usuarios dados de alta hasta el momento.
     */
    public function test(Request $request)
    {
            $users = User::all();
            $response =  response()->json(['sucess' => true, 'value' => $users, "error" => [] ], 200);
     
            return $response;    
    }
    /**
     *  check the health of the API
     *
     * @return json health of the API
     */
    public function health(Request $request)
    {
        return $this -> userService -> health();
    }
}