<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\services\IActivityService;
class SaveRequestResponse
{
    protected $activityService;

    public function __construct(IActivityService $activityService)
    {
        $this->activityService = $activityService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
         $response = $next($request);
         
         $data = [
            'request' => json_encode($request), 
            'response' => json_encode($response)
        ];
        $this -> activityService -> saveActivity($data);
 
         return $response;
    }
}
