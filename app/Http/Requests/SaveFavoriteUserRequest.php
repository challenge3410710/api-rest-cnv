<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class SaveFavoriteUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        {
            return [
                'gif_id' => 'required|string|min:1|max:40',
                'alias' => 'required|string|min:1|max:30',
                'user_id' => 'required|numeric',
            ];
        }
    }

    protected function failedValidation(Validator $validator)
    {  
        $errors = $validator->errors();
        $error = [
            'code' => 400,
            'message' => 'errores de validación',
            'details' => $errors
        ];
        throw new HttpResponseException(response()->json([
            'success' => false,
            'value' => [],
            'error' => $error,
        ], JsonResponse::HTTP_BAD_REQUEST));
    }

    public function messages()
    {
        return [
            'gif_id.required' => 'El gif_id es obligatorio',
            'gif_id.string' => 'El gif_id debe contener sólo valor alfanumérico',
            'gif_id.max' => 'El gif_id no puede tener una longitud mayor a 40',

            'user_id.required' => 'El id de usuario es obligatorio',
            'user_id.numeric' => 'El user_id debe ser numérico',
            
            'alias.required' => 'El alias es obligatorio',     
            'alias.string' => 'El alias debe contener sólo caracteres alfanumericos',
            'alias.max' => 'El alias no puede tener una longitud mayor a 40',
        ];
    }
}
